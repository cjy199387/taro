import Taro, { Component } from '@tarojs/taro'
import { View } from '@tarojs/components'

class Page extends Component {
  renderLoading=() => {
    return(
      <View>loading</View>
    )
  }
  render() {
    const {loading}=this.props
    // console.log(loading)
    return(
      <View>
        {loading
          ?this.renderLoading()
          :this.props.children
        }
      </View>
    )
  }
}

export default Page
