export default {
  namespace: 'global',
  state: {
    isLogin: false,
    counter: 0,
  },
  reducers: {
    save(state, { payload }) {
      return { ...state, ...payload }
    },
  },
  effects: {
    * initTbale({url}, {put}) {
      yield put({
        type: 'save',
        payload: {
          tableUrl: url
        }
      })
      yield put({
        type: 'getTableList',
      })
    },
    * search({val}, {put}) {
      // console.log(val)
      yield put({
        type: 'save',
        payload: {
          tableSearch: val,
          tablePage: 1
        }
      })
      yield put({
        type: 'getTableList',
      })
    },
  },
}
