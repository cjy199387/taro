import global from './global'
import login from '../pages/login/models/login'

export default [
  global,
  login,
]
