import Taro, {Component} from '@tarojs/taro'
import {View, Button, OfficialAccount} from '@tarojs/components'
import {connect} from "@tarojs/redux"
import './index.less'

@connect(s => ({
  ...s['login']
}))
class Page extends Component {
  config = {
    navigationBarTitleText: '录音',
    // navigationStyle: 'custom'
  }

  constructor(props) {
    super(props)
    this.state = {}
  }

  componentDidMount() {}


  componentDidShow() {}

  componentDidHide() {}

  onChange = () => {
    const {dispatch}=this.props
    dispatch({
      type: 'login/save',
      a: 112
    })

  }

  render() {
    const {a, type}=this.props
    return (
      <View className={'index'}>
        <View>{type}</View>
        <View>{a}</View>
        <Button
          onClick={this.onChange}
        >开始录音</Button>
        <OfficialAccount/>
      </View>
    )
  }

  componentWillUnmount() {
  }
}

export default Page
