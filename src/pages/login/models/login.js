
export default {
  namespace: 'login',
  state: {
    type: '1234',
    a: 1,
  },
  reducers: {
    save(state, {type, ...payload}) {
      return { ...state, ...payload }
    },
  },
  effects: {
    *login(e) {
      console.log(e)
    },
  },
}
