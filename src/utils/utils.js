import Taro from "@tarojs/taro"

export const wxConfirm=(content, success, showCancel=true) => {
  // console.log(arguments)
  Taro.showModal({
    content,
    showCancel,
    success({confirm}) {
      confirm&&success()
    }
  })
}

export const wxLoading=(showOrHide=true, text) => {
  if (showOrHide) {
    Taro.showLoading({
      title: text||'加载中',
      mask: true
    })
  }else {
    Taro.hideLoading()
  }
}
